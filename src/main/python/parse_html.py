#!/usr/bin/env python

from lxml.html import parse
from os.path import basename
import argparse
import signal
import sys

# TODO : change to http://docs.python-guide.org/en/latest/scenarios/scrape/
# don't use selenium (to slow for static page)
class ParseTable:
    """Class for parsing html table"""
    def __init__(self, file_path, config=None):
        self.doc = parse(file_path).getroot()
        self.cfg = config


    def parse_column(self, tab, col, top_offset=0, bottom_offset=0, debug=False):
        '''
        For parsing table column
        '''
        # table
        xpath = ''
        contents = []
        count_div = self.doc.xpath("count(//div/table)")
        count_table = self.doc.xpath("count(//table)")

        if count_div < count_table:
            xpath += '//table['+str(tab)+']'
        else:
            xpath += '//div['+str(tab)+']/table'
        table = self.doc.xpath(xpath)

        # row APL - 0002.htm /x:div[4]/x:table/x:tbody/x:tr[1]/x:td[1]
        xpath += "//tr/td["+str(col)+"]/p/span"
        rows = table[0].xpath(xpath)
        for row in rows:
            count_br = row.xpath("count(./br)")
            if count_br > 0:
                contents += row.xpath("./br/preceding::text()[1]")[:1] # 1 data sebelum <br>
                contents += row.xpath("./br/following::text()[1]") # setelah <br>

            else:
                contents += row.xpath("text()")

        if debug: print xpath

        bottom_offset = len(contents)-bottom_offset
        return contents[top_offset:bottom_offset]


    def parse_cell(self, tab_id, col_id, row_id, top_offset=0, bottom_offset=0, debug=False):
        '''
        For parsing table cell
        '''
        # table
        xpath = ''
        contents = []
        count_div = self.doc.xpath("count(//div/table)")
        count_table = self.doc.xpath("count(//table)")

        if count_div < count_table:
            xpath += '//table['+str(tab_id)+']'
        else:
            xpath += '//div['+str(tab_id)+']/table'
        table = self.doc.xpath(xpath)

        # row APL - 0002.htm /x:div[4]/x:table/x:tbody/x:tr[1]/x:td[1]
        xpath += "//tr["+str(row_id)+"]/td["+str(col_id)+"]/p/span"
        rows = table[0].xpath(xpath)
        for row in rows:
            count_br = row.xpath("count(./br)")
            if count_br > 0:
                contents += row.xpath("./br/preceding::text()[1]")[:1] # 1 data sebelum <br>
                contents += row.xpath("./br/following::text()[1]") # setelah <br>
            else:
                contents += row.xpath("text()")

        if debug: print xpath

        bottom_offset = len(contents)-bottom_offset
        return ''.join(contents[top_offset:bottom_offset])

    def parse_as_json(self, fieldnames_cell, fieldnames_column):
        cell = {}
        extra = {
            "File Name": basename(path_file)
        }
        for field in fieldnames_cell:
            try:
                cfg_table = self.cfg.getint(field, 'table')
                cfg_column = self.cfg.getint(field, 'column')
                cfg_row = self.cfg.getint(field, 'row')
                extra = {
                    field: self.parse_cell(cfg_table, cfg_column, cfg_row).encode('utf8')
                }
            except (NoOptionError, NoSectionError, IndexError):
                extra = ""
            cell.update(extra)

        run_once = False
        results = [{}]
        tmp_results = [{}]
        for idy,field in enumerate(fieldnames_column):
            rows = ['']
            try:
                cfg_table = self.cfg.getint(field, 'table')
                cfg_column = self.cfg.getint(field, 'column')
                cfg_top_offset = self.cfg.getint(field, 'top_offset')
                cfg_bottom_offset = self.cfg.getint(field, 'bottom_offset')
                rows = self.parse_column(cfg_table, cfg_column, cfg_top_offset, cfg_top_offset)
            except IndexError:
                pass

            if len(rows) is 0: rows += ['']
            if len(rows) >= len(results):
                results = [{} for i in range(len(rows))]

            for idx,row in enumerate(rows):
                raw_result = row.encode('utf8')
                extra = {field: raw_result}
                # result.update(extra)
                # result.update(cell)
                results[idx].update(extra)
                results[idx].update(cell)
                if run_once:
                    try:
                        results[idx].update(tmp_results[idx])
                    except IndexError:
                        pass
                # sleep(3)

            run_once = True
            tmp_results = results

        return results


if __name__ == '__main__':
    def exit_gracefully(signum, frame):
        # restore the original signal handler as otherwise evil things will happen
        # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
        signal.signal(signal.SIGINT, original_sigint)

        try:
            if raw_input("\nReally quit? (y/n)> ").lower().startswith('y'):
                sys.exit(1)

        except KeyboardInterrupt:
            print("Ok ok, quitting")
            # browser.quit()
            sys.exit(1)

            # restore the exit gracefully handler here
            signal.signal(signal.SIGINT, exit_gracefully)

    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)

    # Parse Argument
    parser = argparse.ArgumentParser(description='Parse html table from abyy finereader result')
    parser.add_argument('url', type=str,
                            help='url of the file (.html)')
    parser.add_argument('table', metavar='i', type=int,
                        help='table i that wanted to get')
    parser.add_argument('column', metavar='c', type=int,
                        help='column c of the table i that wanted to get')
    parser.add_argument('-t', '--top_offset', type=int, default=0,
                            help='top offset')
    parser.add_argument('-b', '--bottom_offset', type=int, default=0,
                            help='bottom offset')
    parser.add_argument('-r', '--row', type=int, default=0,
                            help='specify row')
    args = parser.parse_args()

    pt = ParseTable(args.url)
    if args.row == 0:
        data = pt.parsercolumn(args.table, args.column, args.top_offset, args.bottom_offset)
        for d in data:
            print d
    else:
        print pt.parsecell(args.table, args.column, args.row)
