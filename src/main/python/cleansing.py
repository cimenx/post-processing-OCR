#!/usr/bin/env python coding=utf-8

from dateutil.parser import parse as date_parse
import re

def drop_nonsense(text, minimum_char=5):
    regex = r'\b[a-z|A-Z]{1,}\b'
    text = re.sub(r'\b\d\b', '')
    return None if max(re.findall(regex, text), key=len) < minimum_char else text


# best example file:///home/wildan/Project/IMS/BAD_ALIGNMENT(AFTER_HOMOGRAPHY)/APL/APL%20-%200092.htm
def remove_gibberish(text):
    text = re.sub(r'[^\x00-\x7F]+', '', text) # remove non-ASCII character

    # remove symbol
    text = text.replace('+', '').replace(';','').replace('%','').replace('`','').replace('~','').replace('\\','')
    text = text.replace('*', '').replace('^', '').replace('"', '').replace('/','').replace('|','')
    text = text.replace('?','').replace('>','').replace('<','').replace(',', '').replace(':', '')
    text = re.sub(r'\W*\b[a-z|A-Z]{1,2}\b', '', text) # remove small word

    if len(text.split()) <= 3 and len(text) < 6:
        text = text.replace("'",'').replace("-",'').replace('.', '')
    c1 = re.findall(r'(\b.{1}\b)', text)
    c2 = re.findall(r'([\w|\d]{3,})', text)
    if len(c1) >= 1 and len(c2) == 0:
        text = ''

    text = re.sub(r'(.)\1+', r'\1', text) # reduce recurrent character

    # replace transliteration
    cedilla2latin = [[u'Á', u'A'], [u'á', u'a'], [u'Č', u'C'], [u'č', u'c'], [u'Š', u'S'], [u'š', u's']]
    tr = dict([(a[0], a[1]) for (a) in cedilla2latin])
    new_line = ""
    for letter in text:
        if letter in tr:
            new_line += tr[letter]
        else:
            new_line += letter
    text = new_line

    # recursive cleansing symbol ,-.
    while True:
        last_text = text
        text = ' '.join([word.replace("'",'').replace("-",'').replace('.', '') if len(word) < 3 else word for word in text.split()]) # remove lonely symbol
        if text == last_text: break

    text = text.lstrip().rstrip()
    text = text if len(text) > 2 else None
    return text


def find_price(text):
    result = None
    text = text.replace('o','0').replace('O','0')

    results = re.findall(r'(\d+[,.]\d+(?:[.,]\d+)?)', text)
    if len(results) is 0:
        results = re.findall(r'(\d+[\s]\d+(?:[\s]\d+)?)', text)
        if len(results) is 0:
            try:
                result = max(re.findall(r'\d{3,}', text), key=len)
            except:
                return None
        else:
            result = max(results, key=len).replace(' ','.')
    else:
        result = max(results, key=len).replace('.','').replace(',','.')

    try:
        # TODO: convert currency to float
        return float(result)
    except:
        return None


def find_date(text):
    text = re.sub(r'[oOU]', '0', text)
    # text = text.replace('o','0').replace('O','0').replace('U','0')
    try:
        return date_parse(text, yearfirst=False, dayfirst=True, fuzzy=True)
    except:
        return None
